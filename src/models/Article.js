// import sequelize 
var Sequelize = require('sequelize');
// import connection database
var sequelize = require('../databases/magasin');

var Articles = sequelize.define('article',{

    reference: {
        type: Sequelize.STRING,
        primaryKey: true
    },
    designation: {
        type: Sequelize.STRING,
        unique: true
    },
    type_fabrication_achat: {
        type: Sequelize.STRING,
        allowNull:false
    },
    unite_achat_stock: {
        type: Sequelize.STRING,
        allowNull: false
    },
    delai_en_semaine: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    prix_standard: Sequelize.INTEGER,
    lot_de_reapprovisionnement: Sequelize.INTEGER,
    stock_mini: Sequelize.INTEGER,
    stock_maxi: Sequelize.INTEGER,
    pourcentage_de_perte: Sequelize.INTEGER,
    inventaire: Sequelize.INTEGER,
    pf_ou_mp_ou_pi_ou_se: Sequelize.STRING,

});

module.exports = Articles;