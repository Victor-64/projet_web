// import sequelize 
var Sequelize = require('sequelize');
// import connection database
var sequelize = require('../databases/magasin');

var Articles = require('./Article');
var Operations = sequelize.define('operation',{

    reference: {
        type: Sequelize.STRING,
        primaryKey: true,
        onDelete: 'cascade',
        references: {
            model: Articles,
            key: 'reference'
        },

    },
    numero_operation: {
        type: Sequelize.BIGINT,
        primaryKey: true
    },
    machine_numero_section: {
        type : Sequelize.STRING,
        allowNull: false
    },
    machine_numero_sous_section: {
        type : Sequelize.STRING,
        allowNull: false
    },
    machine_machine: {
        type : Sequelize.STRING,
        allowNull: false
    },
    main_d_oeuvre_numero_section: {
        type : Sequelize.STRING,
        allowNull: false
    },
    main_d_oeuvre_numero_sous_section: {
        type : Sequelize.STRING,
        allowNull: false
    },
    main_d_oeuvre_machine: {
        type : Sequelize.STRING,
        allowNull: false
    },
    temps_preparation: Sequelize.BIGINT,
    temps_execution: Sequelize.BIGINT,
    temps_transfert: Sequelize.BIGINT,
    libelle_operation: Sequelize.STRING,

});

module.exports = Operations;