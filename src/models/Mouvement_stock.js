// import sequelize 
var Sequelize = require('sequelize');
// import connection database
var sequelize = require('../databases/magasin');

var Articles = require('./Article');
var Mouvements_stock = sequelize.define('mouvement_stock',{

    reference: {
        type: Sequelize.STRING,
        primaryKey: true,
        onDelete: 'cascade',
        references: {
            model: Articles,
            key: 'reference'
        },

    },
    numero_magasin: {
        type: Sequelize.BIGINT,
        primaryKey: true
    },
    quantite: {
        type: Sequelize.BIGINT
    },
    periode: {
        type: Sequelize.DATE,
        primaryKey: true
    },
    entree_ou_sortie: {
        primaryKey: true,
        type: Sequelize.BIGINT,
        validate: {
            min: 0,
            max: 1
        }
    }
});

module.exports = Mouvements_stock;