// import sequelize 
var Sequelize = require('sequelize');
// import connection database
var sequelize = require('../databases/magasin');

var Articles = require('./Article');

var Liens_nomenclature = sequelize.define('lien_nomenclature',{

    compose: {
        type: Sequelize.STRING,
        onDelete: 'cascade',
        references: {
            model: Articles,
            key: 'reference'
        },
    },
    composant: {
        type: Sequelize.STRING,
        onDelete: 'cascade',
        references: {
            model: Articles,
            key: 'reference'
        },
        
    },
    quantite_de_composition: {
        type: Sequelize.BIGINT,
        allowNull: false
    }

});

module.exports = Liens_nomenclature;