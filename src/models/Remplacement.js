// import sequelize 
var Sequelize = require('sequelize');
// import connection database
var sequelize = require('../databases/magasin');

var Liens_nomenclature = require('./Lien_nomenclature')
var Remplacements = sequelize.define('remplacement',{

    // Sequelize ne supportant pas les clés étrangères composite, nous sommes obligés de le déclarer ainsi 
    //https://github.com/sequelize/sequelize/issues/311
    //https://github.com/RobinBuschmann/sequelize-typescript/issues/886
    remplace_compose: {
        type : Sequelize.STRING,
        allowNull: false
    },
    remplace_composant: {
        type : Sequelize.STRING,
        allowNull: false
    },
    remplacant_compose: {
        type : Sequelize.STRING,
        allowNull: false
    },
    remplacant_composant: {
        type : Sequelize.STRING,
        allowNull: false
    },
    date_de_remplacement: Sequelize.DATE

});

module.exports = Remplacements;