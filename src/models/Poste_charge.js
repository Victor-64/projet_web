// import sequelize 
var Sequelize = require('sequelize');
// import connection database
var sequelize = require('../databases/magasin');

var Postes_charge = sequelize.define('poste_charge',{

    numero_section: {
        type: Sequelize.BIGINT,
        primaryKey: true
    },
    numero_sous_section: {
        type: Sequelize.BIGINT,
        primaryKey: true
    },
    machine: {
        type: Sequelize.BIGINT,
        primaryKey: true,
        validate: {
            min: 0,
            max: 1
        }
    },
    designation: Sequelize.STRING,
    taux_horaire_ou_forfait: Sequelize.BIGINT,
    nombre_de_postes: Sequelize.BIGINT,
    capacite_nominale: Sequelize.BIGINT,
    type_taux_horaire_ou_forfait: {
        type: Sequelize.STRING,
        validate: {
            isIn: [['TH', 'F']]
        }
    }

});

module.exports = Postes_charge;