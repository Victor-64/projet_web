const controllerMouvement_stock = {}

const req = require('express/lib/request');
// import model
var Mouvements_stock = require('../models/Mouvement_stock');

const { Op } = require("sequelize");


// Services pour les Mouvements de stock : Lecture - Ajout - Lecture Spécifique - Mise à jour - Suppression

controllerMouvement_stock.listMouvement_stock = async ( req, res ) => {
    
    try {
        
        const response = await Mouvements_stock.findAll()
        .then(function(data) {
            const res = { 
                success:true, 
                message: "Load successful",
                data:data}
            return res
        })
        .catch(error=> {
            const res = { 
                sucess: false,
                error: error}
            return res
        })

        res.json(response);

    } catch (e) {
        console.log("Error controller.list");
        console.log(e);
    }
}

controllerMouvement_stock.createMouvement_stock = async ( req, res ) => {

    try {

        const response = await Mouvements_stock.create({ 
            reference: "ES000",
	        numero_magasin: 78952,
	        quantite: 120,
	        periode: new Date(2022, 1, 1),
	        entree_ou_sortie: 0
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Created successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);
    } catch (e) {
        console.log(e);
    }
}

controllerMouvement_stock.updateMouvement_stock = async ( req, res )=> {
    try {

        const refReference = "ES000";

        const response = await Mouvements_stock.update({
	        numero_magasin: 10000,
	        quantite: 580
        },{
            where : {
                reference: refReference
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Updated successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerMouvement_stock.getMouvement_stock = async (req, res) => {

    try {

        const { reference } = req.params;

        const response = await Mouvements_stock.findAll({
            where: {
                reference: reference
            }
        })
        .then(function(data){
            const res = {
                success: true,
                data: data
            }
            return res;
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res;
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerMouvement_stock.deleteMouvement_stock = async ( req, res ) => {

    try {

        const { reference } = req.params;

        const response = await Mouvements_stock.destroy({
            where: {
                reference: reference
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message: "Deleted successful",
                data: data
            }
            return res
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    }catch (e) {
        console.log(e);
    }
}


module.exports=controllerMouvement_stock;