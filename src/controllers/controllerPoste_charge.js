const controllerPoste_charge = {}

const req = require('express/lib/request');
// import model
var Postes_charge = require('../models/Poste_charge');

const { Op } = require("sequelize");


// Services pour Poste_charge : Lecture - Ajout - Lecture Spécifique - Mise à jour - Suppression

controllerPoste_charge.listPoste_charge= async ( req, res ) => {
    
    try {
        
        const response = await Postes_charge.findAll()
        .then(function(data) {
            const res = { 
                success:true, 
                message: "Load successful",
                data:data}
            return res
        })
        .catch(error=> {
            const res = { 
                sucess: false,
                error: error}
            return res
        })

        res.json(response);

    } catch (e) {
        console.log("Error controller.list");
        console.log(e);
    }
}

controllerPoste_charge.createPoste_charge = async ( req, res ) => {

    try {

        const response = await Postes_charge.bulkCreate([{ 
            numero_section: 500,
            numero_sous_section: 450,
            machine: 1,
            designation:"Rectifieuse",
            taux_horaire_ou_forfait: 80, 
            nombre_de_postes: 1,
            capacite_nominale: 39,
            type_taux_horaire_ou_forfait: 'TH'
        },
        {
            numero_section: 500,
            numero_sous_section: 450,
            machine: 0,
            designation:"Rectifieur",
            taux_horaire_ou_forfait: 80, 
            nombre_de_postes: 1,
            capacite_nominale: 39,
            type_taux_horaire_ou_forfait: 'TH'
        },
        ])
        .then(function(data){
            const res = {
                success: true,
                message:"Created successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);
    } catch (e) {
        console.log(e);
    }
}

controllerPoste_charge.updatePoste_charge = async ( req, res )=> {
    try {

        const refnumero_section = 500;

        const response = await Postes_charge.update({
            designation:"Cisailleur",
            taux_horaire_ou_forfait: 80, 
            nombre_de_postes: 1,
            capacite_nominale: 39,
            type_taux_horaire_ou_forfait: 'TH'
        },{
            where : {
                numero_section: refnumero_section
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Updated successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerPoste_charge.getPoste_charge = async (req, res) => {

    try {

        const { numero_section } = req.params;

        const response = await Postes_charge.findAll({
            where: {
                numero_section: numero_section
            }
        })
        .then(function(data){
            const res = {
                success: true,
                data: data
            }
            return res;
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res;
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerPoste_charge.deletePoste_charge = async ( req, res ) => {

    try {

        const { machine } = req.params;

        const response = await Postes_charge.destroy({
            where: {
                machine: machine
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message: "Deleted successful",
                data: data
            }
            return res
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    }catch (e) {
        console.log(e);
    }
}


module.exports=controllerPoste_charge;