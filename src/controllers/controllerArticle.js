const controllerArticle = {}

const req = require('express/lib/request');
// import model
var Articles = require('../models/Article');

const { Op } = require("sequelize");


// Services pour l'Article : Lecture - Ajout - Lecture Spécifique - Mise à jour - Suppression

controllerArticle.listArticle = async ( req, res ) => {
    
    try {
        
        const response = await Articles.findAll()
        .then(function(data) {
            const res = { 
                success:true, 
                message: "Load successful",
                data:data}
            return res
        })
        .catch(error=> {
            const res = { 
                sucess: false,
                error: error}
            return res
        })

        res.json(response);

    } catch (e) {
        console.log("Error controller.list");
        console.log(e);
    }
}

controllerArticle.createArticle = async ( req, res ) => {

    try {

        const response = await Articles.bulkCreate([{ 
            reference: "ROUE50",
            designation: "roue de camion",
            type_fabrication_achat:"achat par lot",
            unite_achat_stock:"unite",
            delai_en_semaine: 6, 
            prix_standard: 2,
            lot_de_reapprovisionnement: 500,
            stock_mini: 500,
            stock_maxi: 2000,
            pourcentage_de_perte: null,
            inventaire: 1850,
            pf_ou_mp_ou_pi_ou_se: "pi"
        },
        {
            reference: "ES000",
            designation: "essieu monte",
            type_fabrication_achat:"fabr. par lot",
            unite_achat_stock:"unite",
            delai_en_semaine: 2, 
            prix_standard: null,
            lot_de_reapprovisionnement: 500,
            stock_mini: 750,
            stock_maxi: 1500,
            pourcentage_de_perte: null,
            inventaire: null,
            pf_ou_mp_ou_pi_ou_se: "se"
        },
        {
            reference: "CH005",
            designation: "chassis monte",
            type_fabrication_achat:"fabr. par lot",
            unite_achat_stock:"unite",
            delai_en_semaine: 1, 
            prix_standard: null,
            lot_de_reapprovisionnement: 300,
            stock_mini: null,
            stock_maxi: 900,
            pourcentage_de_perte: null,
            inventaire: null,
            pf_ou_mp_ou_pi_ou_se: "se"
        },
        {
            reference: "H000",
            designation: "conteneur bleu",
            type_fabrication_achat:"fabr. par lot",
            unite_achat_stock:"unite",
            delai_en_semaine: 1, 
            prix_standard: null,
            lot_de_reapprovisionnement: 150,
            stock_mini: 350,
            stock_maxi: 800,
            pourcentage_de_perte: null,
            inventaire: null,
            pf_ou_mp_ou_pi_ou_se: "se"
        },
        {
            reference: "H001",
            designation: "conteneur bleu special",
            type_fabrication_achat:"fabr. a la commande",
            unite_achat_stock:"unite",
            delai_en_semaine: 1, 
            prix_standard: null,
            lot_de_reapprovisionnement: 150,
            stock_mini: 350,
            stock_maxi: null,
            pourcentage_de_perte: null,
            inventaire: null,
            pf_ou_mp_ou_pi_ou_se: "se"
        },
        {
            reference: "CD100",
            designation: "camion demenagement bleu",
            type_fabrication_achat:"fabr. par lot",
            unite_achat_stock:"unite",
            delai_en_semaine: 2, 
            prix_standard: null,
            lot_de_reapprovisionnement: 200,
            stock_mini: null,
            stock_maxi: 600,
            pourcentage_de_perte: null,
            inventaire: null,
            pf_ou_mp_ou_pi_ou_se: "pf"
        }
        ])
        .then(function(data){
            const res = {
                success: true,
                message:"Created successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);
    } catch (e) {
        console.log(e);
    }
}

controllerArticle.updateArticle = async ( req, res )=> {
    try {

        const refArticle = "ROUE50";

        const response = await Articles.update({
            designation: "roue de tracteur",
            type_fabrication_achat:"achat par lot",
            unite_achat_stock:"unite",
            delai_en_semaine: 6, 
            prix_standard: 2,
            lot_de_reapprovisionnement: 500,
            stock_mini: 700,
            stock_maxi: 1500,
            pourcentage_de_perte: null,
            inventaire: 1850,
            pf_ou_mp_ou_pi_ou_se: "pf"
        },{
            where : {
                reference: refArticle
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Updated successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerArticle.getArticle = async (req, res) => {

    try {

        const { reference } = req.params;

        const response = await Articles.findAll({
            /*where: {
                reference: reference
            }*/
            where: {
                designation: {
                    [Op.like]: "%bleu%"
                }
            }
        })
        .then(function(data){
            const res = {
                success: true,
                data: data
            }
            return res;
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res;
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerArticle.deleteArticle = async ( req, res ) => {

    try {

        const { reference } = req.params;

        const response = await Articles.destroy({
            where: {
                reference: reference
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message: "Deleted successful",
                data: data
            }
            return res
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    }catch (e) {
        console.log(e);
    }
}


module.exports=controllerArticle;