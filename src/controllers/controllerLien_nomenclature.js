const controllerLien_nomenclature = {}

const req = require('express/lib/request');
// import model
var Liens_nomenclature = require('../models/Lien_nomenclature');

const { Op } = require("sequelize");


// Services pour les Liens de nomenclature : Lecture - Ajout - Lecture Spécifique - Mise à jour - Suppression

controllerLien_nomenclature.listLien = async ( req, res ) => {
    
    try {
        
        const response = await Liens_nomenclature.findAll()
        .then(function(data) {
            const res = { 
                success:true, 
                message: "Load successful",
                data:data}
            return res
        })
        .catch(error=> {
            const res = { 
                sucess: false,
                error: error}
            return res
        })

        res.json(response);

    } catch (e) {
        console.log("Error controller.list");
        console.log(e);
    }
}

controllerLien_nomenclature.createLien = async ( req, res ) => {

    try {

        const response = await Liens_nomenclature.bulkCreate([{ 
            compose: "ES000",
            composant: "ROUE50",
            quantite_de_composition: 2
        },
        {
            compose: "CH005",
            composant: "ES000",
            quantite_de_composition: 2
        },
        {
            compose: "CD100",
            composant: "CH005",
            quantite_de_composition: 1
        },
        {
            compose: "CD100",
            composant: "H000",
            quantite_de_composition: 1
        },
        {
            compose: "CD100",
            composant: "H001",
            quantite_de_composition: 1
        }
        ])
        .then(function(data){
            const res = {
                success: true,
                message:"Created successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);
    } catch (e) {
        console.log(e);
    }
}

controllerLien_nomenclature.updateLien = async ( req, res )=> {
    try {

        const refCompose = "CH005";

        const response = await Liens_nomenclature.update({
            compose: "CD100",
            composant: "ES000",
            quantite_de_composition: 1
        },{
            where : {
                compose: refCompose
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Updated successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerLien_nomenclature.getLien = async (req, res) => {

    try {

        const { quantite_de_composition } = req.params;

        const response = await Liens_nomenclature.findAll({
            where: {
                quantite_de_composition: quantite_de_composition
            }
        })
        .then(function(data){
            const res = {
                success: true,
                data: data
            }
            return res;
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res;
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerLien_nomenclature.deleteLien = async ( req, res ) => {

    try {

        const { compose } = req.params;

        const response = await Liens_nomenclature.destroy({
            where: {
                compose: compose
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message: "Deleted successful",
                data: data
            }
            return res
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    }catch (e) {
        console.log(e);
    }
}


module.exports=controllerLien_nomenclature;