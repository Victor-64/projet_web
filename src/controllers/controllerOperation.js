const controllerOperation = {}

const req = require('express/lib/request');
// import model
var Operations = require('../models/Operation');

const { Op } = require("sequelize");


// Services pour les Opérations : Lecture - Ajout - Lecture Spécifique - Mise à jour - Suppression

controllerOperation.listLien = async ( req, res ) => {
    
    try {
        
        const response = await Operations.findAll()
        .then(function(data) {
            const res = { 
                success:true, 
                message: "Load successful",
                data:data}
            return res
        })
        .catch(error=> {
            const res = { 
                sucess: false,
                error: error}
            return res
        })

        res.json(response);

    } catch (e) {
        console.log("Error controller.list");
        console.log(e);
    }
}

controllerOperation.createLien = async ( req, res ) => {

    try {

        const response = await Operations.Create({ 
            /*reference: ,
	        numero_operation: , 
	        machine: ,
	        main_d_oeuvre: ,
	        temps_preparation: , 
	        temps_execution: ,
	        temps_transfert: ,
	        libelle_operation: */
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Created successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);
    } catch (e) {
        console.log(e);
    }
}

controllerOperation.updateLien = async ( req, res )=> {
    try {

        const refRemplacement = "???";

        const response = await Operations.update({
            /*reference: ,
	        numero_operation: , 
	        machine: ,
	        main_d_oeuvre: ,
	        temps_preparation: , 
	        temps_execution: ,
	        temps_transfert: ,
	        libelle_operation: */
        },{
            where : {
                remplace: refRemplacement
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Updated successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerOperation.getLien = async (req, res) => {

    try {

        const { reference } = req.params;

        const response = await Operations.findAll({
            where: {
                reference: reference
            }
        })
        .then(function(data){
            const res = {
                success: true,
                data: data
            }
            return res;
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res;
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerOperation.deleteLien = async ( req, res ) => {

    try {

        const { reference } = req.params;

        const response = await Operations.destroy({
            where: {
                reference: reference
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message: "Deleted successful",
                data: data
            }
            return res
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    }catch (e) {
        console.log(e);
    }
}


module.exports=controllerOperation;