const controllerRemplacement = {}

const req = require('express/lib/request');
// import model
var Remplacements = require('../models/Remplacement');

const { Op } = require("sequelize");


// Services pour les remplacements : Lecture - Ajout - Lecture Spécifique - Mise à jour - Suppression

controllerRemplacement.listLien = async ( req, res ) => {
    
    try {
        
        const response = await Remplacements.findAll()
        .then(function(data) {
            const res = { 
                success:true, 
                message: "Load successful",
                data:data}
            return res
        })
        .catch(error=> {
            const res = { 
                sucess: false,
                error: error}
            return res
        })

        res.json(response);

    } catch (e) {
        console.log("Error controller.list");
        console.log(e);
    }
}

controllerRemplacement.createLien = async ( req, res ) => {

    try {

        const response = await Remplacements.Create({ 
            //remplace: ???,
            //remplacant: ???,
            //date_de_remplacement: ???
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Created successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);
    } catch (e) {
        console.log(e);
    }
}

controllerRemplacement.updateLien = async ( req, res )=> {
    try {

        const refRemplacement = "???";

        const response = await Remplacements.update({
            //remplace: ???,
            //remplacant: ???,
            //date_de_remplacement: ???
        },{
            where : {
                remplace: refRemplacement
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message:"Updated successful",
                data: data
            }
            return res;
        })
        .catch(error=> {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerRemplacement.getLien = async (req, res) => {

    try {

        const { remplace } = req.params;

        const response = await Remplacements.findAll({
            where: {
                remplace: remplace
            }
        })
        .then(function(data){
            const res = {
                success: true,
                data: data
            }
            return res;
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res;
        })

        res.json(response);

    } catch (e) {
        console.log(e);
    }
}

controllerRemplacement.deleteLien = async ( req, res ) => {

    try {

        const { remplace } = req.params;

        const response = await Remplacements.destroy({
            where: {
                remplace: remplace
            }
        })
        .then(function(data){
            const res = {
                success: true,
                message: "Deleted successful",
                data: data
            }
            return res
        })
        .catch(error => {
            const res = {
                success: false,
                error: error
            }
            return res
        })

        res.json(response);

    }catch (e) {
        console.log(e);
    }
}


module.exports=controllerRemplacement;