var express = require('express');
var route = express();
// import controller
const controllerArticle = require('./controllers/controllerArticle');
const controllerLien_nomenclature = require('./controllers/controllerLien_nomenclature');
const controllerRemplacement = require('./controllers/controllerRemplacement');
const controllerPoste_charge = require('./controllers/controllerPoste_charge');
const controllerOperation = require('./controllers/controllerOperation');
const controllerMouvement_stock = require('./controllers/controllerMouvement_stock');

// create route

route.get("/listArticle", controllerArticle.listArticle);
route.get("/createArticle", controllerArticle.createArticle);
route.get("/updateArticle", controllerArticle.updateArticle);
route.get("/getArticle/:reference", controllerArticle.getArticle);
route.get("/deleteArticle/:reference", controllerArticle.deleteArticle);

route.get("/listLien", controllerLien_nomenclature.listLien);
route.get("/createLien", controllerLien_nomenclature.createLien);
route.get("/updateLien", controllerLien_nomenclature.updateLien);
route.get("/getLien/:compose", controllerLien_nomenclature.getLien);
route.get("/deleteLien/:compose", controllerLien_nomenclature.deleteLien);

/*route.get("/listRemplacement", controllerRemplacement.listRemplacement);
route.get("/createRemplacement", controllerRemplacement.createRemplacement);
route.get("/updateRemplacement", controllerRemplacement.updateRemplacement);
route.get("/getRemplacement/:remplace", controllerRemplacement.getRemplacement);
route.get("/deleteRemplacement/:remplace", controllerRemplacement.deleteRemplacement);*/

route.get("/listPoste_charge", controllerPoste_charge.listPoste_charge);
route.get("/createPoste_charge", controllerPoste_charge.createPoste_charge);
route.get("/updatePoste_charge", controllerPoste_charge.updatePoste_charge);
route.get("/getPoste_charge/:numero_section", controllerPoste_charge.getPoste_charge);
route.get("/deletePoste_charge/:machine", controllerPoste_charge.deletePoste_charge);

/*route.get("/listOperation", controllerOperation.listOperation);
route.get("/createOperation", controllerOperation.createOperation);
route.get("/updateOperation", controllerOperation.updateOperation);
route.get("/getOperation/:numero_section", controllerOperation.getOperation);
route.get("/deleteOperation/:machine", controllerOperation.deleteOperation);*/

route.get("/listMouvement_stock", controllerMouvement_stock.listMouvement_stock);
route.get("/createMouvement_stock", controllerMouvement_stock.createMouvement_stock);
route.get("/updateMouvement_stock", controllerMouvement_stock.updateMouvement_stock);
route.get("/getMouvement_stock/:reference", controllerMouvement_stock.getMouvement_stock);
route.get("/deleteMouvement_stock/:reference", controllerMouvement_stock.deleteMouvement_stock);

// export route
module.exports = route;