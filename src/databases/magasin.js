const Sequelize = require('sequelize');

const database = new Sequelize(
    'dapm0d3221p9de', // name database
    'vfdnszimzfakdp', // user database
    '971735d571c7baa9c782382b11d3f45770d23bac02d4338afb5a8008c5495288', // password database
    {
        host: 'ec2-54-195-141-170.eu-west-1.compute.amazonaws.com',
        dialect: 'postgres', // mariadb / sqlite / postgres
        protocol: 'postgres',
        dialectOptions: {
            ssl: {
                require: true,
                rejectUnauthorized: false
            }
        }
    }
);
database.sync();

module.exports = database;