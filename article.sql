DROP TABLE IF EXISTS Lien_de_nomenclature_;
DROP TABLE IF EXISTS Remplacement_;
DROP TABLE IF EXISTS Poste_de_charge_;
DROP TABLE IF EXISTS Operation_;
DROP TABLE IF EXISTS Mouvement_de_stock_;
DROP TABLE IF EXISTS Article_;


DROP TYPE IF EXISTS Article;
DROP TYPE IF EXISTS Remplacement;
DROP TYPE IF EXISTS Operation;
DROP TYPE IF EXISTS Mouvement_de_stock;
DROP TYPE IF EXISTS Lien_de_nomenclature;
DROP TYPE IF EXISTS Poste_de_charge;

CREATE TYPE Article as (
    reference varchar(30),
	designation varchar(30),
	type_fabrication_achat varchar(30),
	unite_achat_stock varchar(30),
	delai_en_semaine bigint,
	prix_standard bigint,
	lot_de_reapprovisionnement bigint,
	stock_mini bigint,
	stock_maxi bigint,
	pourcentage_de_perte bigint,
	inventaire bigint,
	PF_ou_MP_ou_Pi_ou_SE varchar(2)
);

CREATE TYPE Lien_de_nomenclature as(
	compose varchar(30),
	composant varchar(30),
	quantite_de_composition bigint
);

CREATE TYPE Remplacement as (
	remplace Lien_de_nomenclature,
	remplacant Lien_de_nomenclature,
	date_de_remplacement timestamp with time zone
);

CREATE TYPE Poste_de_charge as (
	numero_section bigint,
	numero_sous_section bigint,
	machine bigint,
	designation varchar(30),
	taux_horaire_ou_forfait bigint,
	nombre_de_postes bigint,
	capacite_nominale bigint,
	type_taux_horaire_ou_forfait varchar(2)
);

CREATE TYPE Operation as (
	reference varchar(30),
	numero_operation bigint,
	machine Poste_de_charge,
	main_d_oeuvre Poste_de_charge,
	temps_preparation bigint,
	temps_execution bigint,
	temps_transfert bigint,
	libelle_operation varchar(30)
);

CREATE TYPE Mouvement_de_stock as(
	reference varchar(30),
	numero_magasin bigint,
	quantite bigint,
	periode timestamp with time zone,
	entree_ou_sortie bigint
);

/* TABLES */ 

CREATE TABLE Article_ of Article(
	constraint Article_pk primary key(reference),
	constraint Article_unique unique(designation),
	constraint Article_nn_1 check (type_fabrication_achat is not null),
    constraint Article_nn_2 check (unite_achat_stock is not null),
	constraint Article_nn_3 check (delai_en_semaine is not null)
);

CREATE TABLE Lien_de_nomenclature_ of Lien_de_nomenclature(
	constraint Lien_de_nomenclature_pk primary key(compose,composant),
	constraint Lien_de_nomenclature_fk_1 foreign key(compose) references Article_(reference) on delete cascade,
	constraint Lien_de_nomenclature_fk_2 foreign key(composant) references Article_(reference) on delete cascade,
	constraint Lien_de_nomenclature_nn check (quantite_de_composition is not null)
);

CREATE TABLE Remplacement_ of Remplacement(
-- constraint Remplacement_unique unique(remplace,remplacant),
	constraint Remplacement_nn_1 check (remplace is not null),
	constraint Remplacement_nn_2 check (remplacant is not null),
	constraint Remplacement_check check(remplace <> remplacant)
);

CREATE TABLE Poste_de_charge_ of Poste_de_charge(
	constraint Poste_de_charge_pk primary key(numero_section,numero_sous_section,machine),
	constraint Poste_de_charge_check_1 check(machine = 0 or machine = 1),
	constraint Poste_de_charge_check_2 check(type_taux_horaire_ou_forfait = 'TH' or type_taux_horaire_ou_forfait = 'F')
);

CREATE TABLE Operation_ of Operation(
	constraint Operation_pk primary key(reference,numero_operation),
	constraint Operation_fk1 foreign key(reference) references Article_(reference) on delete cascade,
	constraint Operation_check check(machine <> main_d_oeuvre)
); 

CREATE TABLE Mouvement_de_stock_ of Mouvement_de_stock(
	constraint Mouvement_de_stock_pk primary key(reference,periode,entree_ou_sortie),
	constraint Mouvement_de_stock_fk foreign key(reference) references Article_(reference) on delete cascade,
	constraint Mouvement_de_stock_check check(entree_ou_sortie = 0 or entree_ou_sortie = 1)
);

/* INSERT DATAS */

insert into Article_ values('ROUE50','roue de camion','achat par lot','unite',6,1.5,500,500,2000,null,1850,'Pi');
insert into Article_ values('ES000','essieu monte','fabr. par lot','unite',2,null,500,750,1500,null,null,'SE');
insert into Article_ values('CH005','chassis monte','fabr. par lot','unite',1,null,300,null,900,null,null,'SE');

insert into Article_ values('H000','conteneur bleu','fabr. par lot','unite',1,null,150,350,800,null,null,'SE');
insert into Article_ values('H001','conteneur bleu special','fabr. a la commande','unite',1,null,150,350,null,null,null,'SE');

insert into Article_ values('CD100','camion demenagement bleu','pf fabr. par lot','unite',2,null,200,null,600,null,null,'PF');

insert into Lien_de_nomenclature_ values('ES000','ROUE50',2);
insert into Lien_de_nomenclature_ values('CH005','ES000',2);
insert into Lien_de_nomenclature_ values('CD100','CH005',1);

insert into Lien_de_nomenclature_ values('CD100','H000',1);
insert into Lien_de_nomenclature_ values('CD100','H001',1);

insert into Remplacement_ select (ln1),(ln2),current_timestamp from Lien_de_nomenclature_ ln1,Lien_de_nomenclature_ ln2 where
	ln1.compose = 'CD100' and ln2.compose = 'CD100' and ln1.composant = 'H000' and ln2.composant = 'H001';

insert into Poste_de_charge_ values(500,450,1,'Rectifieuse',80,1,39,'TH');
insert into Poste_de_charge_ values(500,450,0,'Rectifieur',80,1,39,'TH');

insert into Operation_ select 'ES000',020,(machines),(main_d_oeuvre),0.5,0.05,0.2,'Rectification' from
Poste_de_charge_ machines,Poste_de_charge_ main_d_oeuvre where machines.designation = 'Rectifieuse' and main_d_oeuvre.designation = 'Rectifieur';